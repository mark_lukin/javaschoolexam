package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null) {
            throw new IllegalArgumentException();
        }

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();

        Double n = (-1 + Math.sqrt(1 + 8 * size)) / 2;
        int rows;

        if (n % 1 == 0) {
            rows = n.intValue();
        } else {
            throw new CannotBuildPyramidException();
        }

        int columns = 2 * rows - 1;

        Collections.sort(inputNumbers);
        Iterator<Integer> it = inputNumbers.iterator();

        int[][] pyramid = new int[rows][columns];

        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < rows - i - 1; ++j)
                pyramid[i][j] = 0;

            for (int j = rows - i - 1; j < rows + i; j+=2) {
                pyramid[i][j] = it.next();
            }

            for (int j = rows - i; j < rows + i; j+=2) {
                pyramid[i][j] = 0;
            }

            for(int j = rows + i; j < columns; ++j)
                pyramid[i][j] = 0;
        }

        return pyramid;
    }
}
